const api_url = "http://localhost:8080/api/cart";

// Defining async function
async function getApi(url) {
  // Storing response
  const response = await fetch(url);
  // Storing data in form of JSON
  var data = await response.json();
  show(data);
}
// Calling that async function
getApi(api_url);
// Function to define innerHTML for HTML table

function show(data) {
  // Loop to access all rows
  var html = "";
  for (let item of data) {
    html += ` 
    <tr>
    <td>${item.product.productName}</td>
    <td><span>${item.quantity}</span></td>
    <td><span>${item.product.unitPrice}</span></td>
    <td><span>${item.quantity * item.product.unitPrice}</span></td>
    <td>
        <button id="remove" onclick="onRemove(${item.id})">Remove</button>
    </td>
  </tr>
     `;
  }
  var cart = document.getElementById("table-list-cart");
  cart.innerHTML += html;

  //total
  var total = 0;
  for (var item of data) {
    total += item.quantity * item.product.unitPrice;
  }

  var totalPrices = document.getElementById("total");
  totalPrices.innerHTML += `
    <tr>
    <td id="product-name"></td>
    <td id="quantity"></td>
    <td id="unit-price">Grand Total</td>
    <td id="prices"><span>${total}</span></td>
    <td id="action"></td>
    </tr>
  `;
}

async function onRemove(id) {
  axios.delete(`http://localhost:8080/api/cart/${id}`);
}
